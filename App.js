import React, {useState} from 'react';
import {Home, Contact, About} from './src/Pages';
import {Navbar} from './src/components';

const App = () => {
  const [active, setActive] = useState(0);
  const tabData = ['Home', 'Contact', 'About'];
  const toggleSwitch = index => {
    setActive(index);
  };

  return (
    <>
      <Navbar tabData={tabData} active={active} toggle={toggleSwitch} />
      {active === 0 && <Home />}
      {active === 1 && <Contact />}
      {active === 2 && <About />}
    </>
  );
};

export default App;
